"""A lion"""
import animal
class Lion(animal.Animal):
    def _init__(self):
        self.kind = 'lion'
    def get_kind(self):
        return self.kind
